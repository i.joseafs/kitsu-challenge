# Kitsu - Challenge

- O projeto consiste em fazer uma SPA (Single Page Application) responsiva dividida em duas partes, para mostrar informações sobre os Animes atuais com Kitsu.API


## Missão

- Desenvolver uma tela com Html/Css/Js ou React.
- Código mais simples possível.


## Listagem

- Mostra uma lista com os personagens e suas descrições.
- A listagem deve ter 10 personagens por página.
- Para telas pequenas, mostrar apenas os personagens, sem suas descrições.
- Deve ser possível filtrar os personagens por nome
- Deve ser possível navegar entre as páginas da tabela
- Ao selecionar um personagem, mostrar as informações sobre ele em na parte de Detalhe
- Seguir a especificação em anexo


## Detalhes

- Deve ter pelo menos uma listagem de mídias que o personagem participou, com título e imagem. Você pode adicionar o que mais achar relevante e interessante.
- Layout livre. Use a criatividade.


## Ferramentas

- Para criar o App, você deve utilizar as APIs que a Kitsu disponibiliza: https://kitsu.docs.apiary.io.

- Para listar os personagens: https://kitsu.docs.apiary.io/#reference/characters-&-people/characters.

- Para listar as mídias: No json do personagem, em relationships > mediaCharacters > links > related. Acessando esse caminho, ele retornará à listagem de links da mídia e, nela, terá a "relationships > media > links > related”, com todas as informações necessárias da mídia.


## Observações sobre a especificação

- Deverá ter uma coluna com a descrição do personagem.
- Deverá haver um método de buscar.


## Cronograma do projeto
- 3 dias.


# Happy coding!