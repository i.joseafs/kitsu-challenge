const o_content = document.getElementById('tb-list');
const o_mdl = document.getElementById('mdl-view');
const o_nav = document.getElementById('list-nav');

/* Just for Page button be like on layout... */
var w_width;
var body = document.getElementsByTagName('BODY')[0];
w_width = document.body.clientWidth;
body.onresize = function(){
    w_width = document.body.clientWidth;
};


/* Table */
function ins_table(i){

    /* Create DOM */
    const o_tr = document.createElement('tr');
    o_tr.setAttribute('class', 't-list');
    o_tr.setAttribute('height', '112px');
    
    const o_title = document.createElement('td');
    o_title.setAttribute('class','t-title');
    o_title.textContent = i.attributes.name;
    
    const o_info = document.createElement('td');
    o_info.setAttribute('class','t-info');
    o_info.innerHTML = `${i.attributes.description.substring(0,200)}...`;
    
    const o_link = document.createElement('button');
    o_link.setAttribute('class','t-link');
    o_link.setAttribute('onclick', "i_get_id("+i.id+")");

    const o_d_img = document.createElement('div');
    o_d_img.setAttribute('class','t-img');
    const o_img = document.createElement('img');
    if(i.attributes.image == null){
        o_img.src = 'library/img/broken-img.svg';
    } else {
        o_img.src = i.attributes.image.original;
    }
    o_img.title = i.attributes.name;
    o_img.width = 120;

    /* ins_tableing Fields */
    o_d_img.appendChild(o_img);
    o_title.appendChild(o_d_img);
    o_tr.appendChild(o_title);
    o_info.appendChild(o_link);
    o_tr.appendChild(o_info);
    
    o_content.appendChild(o_tr);
}

function ins_pagination_old(i){
    
    const o_first = document.getElementById('btn-first');
    o_first.setAttribute('onclick', "i_get_list('','','','"+i.first+"')");

    const o_last = document.getElementById('btn-last');
    o_last.setAttribute('onclick', "i_get_list('','','','"+i.last+"')");

    const o_prev = document.getElementById('btn-prev');
    if(i.prev){
        o_prev.setAttribute('onclick', "i_get_list('','','','"+i.prev+"')");
        o_prev.removeAttribute("disabled");
        o_first.removeAttribute("disabled");
    } else {
        o_prev.setAttribute("disabled",true);
        o_first.setAttribute("disabled",true);
    }
    
    const o_next = document.getElementById('btn-next');
    if(i.next){
        o_next.setAttribute('onclick', "i_get_list('','','','"+i.next+"')");
        o_next.removeAttribute("disabled");
        o_last.removeAttribute("disabled");
    } else {
        o_next.setAttribute("disabled",true);
        o_last.setAttribute("disabled",true);
    }
}

function ins_pagination(i){

    var pg_filter = '';
    
    if(!i.links.next){
        console.log('Causa do erro: Não houve retorno de um link.next');
    }
    
    ary = i.links.next.split('&');
    ary_s = ary[1].search('offset%5D=');
    if(ary_s > 0){
        pg_offset = ary[1].substring(ary_s+10);
    } else {
        ary_s = ary[2].search('offset%5D=');
        pg_offset = ary[2].substring(ary_s+10);
    }

    ary_nm = ary[0].search('name%5D=');
    if(ary_nm > 0){
        pg_filter = ary[0].substring(ary_nm+8);
    }
    
    pg_max = 10;
    pg_nav = Math.ceil(pg_offset / 10);
    pg_lin = i.meta.count;
    pg_limit = Math.ceil(i.meta.count / pg_max);
    pg_nav = (pg_nav == 0)? 1 : pg_nav;
    pg_nav = (pg_nav > pg_limit && pg_limit != 0)?  pg_limit : pg_nav;
    pg_start = (pg_nav == 1)? 0 : (pg_nav-1) * pg_max;

    var btn_prev = (pg_nav - 2) * 10;
    var btn_next = pg_nav * 10;

    o_nav.innerHTML = '';

    /* Numbers mask */
    if(w_width > 763){
        n_min = 3;
        n_next = 3;
        n_prev = 2;
        n_limit = 6;
    } else {
        n_min = 1;
        n_next = 2;
        n_prev = 1;
        n_limit = 4;
    }

    if(pg_nav > n_min){
        pg_next = (pg_limit > pg_nav)? n_next : 0;
        pg_next+= pg_nav;
        pg_prev = pg_nav - n_prev;
    } else {
        pg_prev = 1;
        pg_next = (pg_limit > n_limit)? n_limit : pg_limit;
    }
    for(k=pg_prev; k < pg_next; k++){
        const o_rf = document.createElement('button');
        const o_rf_n = document.createElement('div');

        if(pg_nav == k){
            o_rf.classList.add('in');
        }
        o_rf.classList.add('nav-btn');
        o_rf_n.textContent = k;
        pg_k = (k-1) * 10;
        o_rf.setAttribute('onclick', `i_get_list('${pg_max}','${pg_k}','${pg_filter}')`);

        o_rf.appendChild(o_rf_n);
        o_nav.appendChild(o_rf);
    }
    
    const o_prev = document.getElementById('btn-prev');
    if(i.links.prev){
        o_prev.setAttribute('onclick', `i_get_list('${pg_max}','${btn_prev}','${pg_filter}')`);
        o_prev.removeAttribute("disabled");
    } else {
        o_prev.setAttribute("disabled",true);
    }
    
    const o_next = document.getElementById('btn-next');
    if(i.links.next){
        o_next.setAttribute('onclick', `i_get_list('${pg_max}','${btn_next}','${pg_filter}')`);
        o_next.removeAttribute("disabled");
    } else {
        o_next.setAttribute("disabled",true);
    }
}

function i_get_list(limit, offset, filter, full){
    if(full){
        var u_base = full;
    } else {
        var u_base = 'https://kitsu.io/api/edge/characters?';
        if(limit){
            u_base += 'page[limit]='+limit;
        } 
        if(offset){
            u_base += '&page[offset]='+offset;
        } 
        if(filter){
            u_base += '&filter[name]='+filter;
        } 
    }

    var i_rq = new XMLHttpRequest();
    i_rq.onload = function() {
        if (this.status == 200) {

            
            /* Convert into Array Json */
            var list = JSON.parse(this.response);
            data = list.data;
            
            /* Pagination */
            ins_pagination(list);

            /* Clear before insert */
            o_content.innerHTML = '';

            /* Insert at table */
            data.forEach(ary => {
                ins_table(ary);
            });
        } else {
            console.log('Something Wrong');
        }
    };
    i_rq.open('GET', u_base, true);
    i_rq.send();
};

/* Modal */
function ins_mdl_media(i){
    o_mdl.classList.add('in');

    const o_title = document.getElementById('mdl-a-title');
    o_title.textContent = i.attributes.canonicalTitle;
    
    const o_info = document.getElementById('mdl-a-info');
    o_info.innerHTML = i.attributes.synopsis;

    const o_age = document.getElementById('mdl-a-age');
    if(i.attributes.ageRatingGuide == ''){
        o_age.textContent = 'Not Available';
    } else {
        o_age.textContent = i.attributes.ageRatingGuide;
    }
    const o_imdb = document.getElementById('mdl-a-imdb');
    if(i.attributes.averageRating == null){
        o_imdb.textContent = 'Not Available';
    } else {
        o_imdb.textContent = i.attributes.averageRating;
    }

    const o_status = document.getElementById('mdl-a-status');
    if(i.attributes.ageRatingGuide == null){
        o_status.textContent = 'Not Available';
    } else {
        o_status.textContent = i.attributes.status;
    }
    
    const o_epi = document.getElementById('mdl-a-epi');
    if(i.attributes.episodeLength == null){
        o_epi.textContent = 'Not Available';
    } else {
        o_epi.textContent = i.attributes.episodeLength;
    }


    const o_img = document.getElementById('mdl-a-img');
    o_img.width = 250;
    if(i.attributes.posterImage == null){
        o_img.src = 'library/img/broken-img.svg';
    } else {
        o_img.src = i.attributes.posterImage.medium;
    }
    o_img.title = i.attributes.canonicalTitle;
}

function i_get_media(id){
    var u_base = 'https://kitsu.io/api/edge/media-characters/'+id+'/media';
    var i_rq = new XMLHttpRequest();
    i_rq.onload = function() {
        if (this.status == 200) {
            var info = JSON.parse(this.response);

            data = info.data;
            ins_mdl_media(data);

        } else {
            console.log("Some Error... ?");
        }
    };
    i_rq.open('GET', u_base, true);
    i_rq.send();
}
function i_get_relation(id){
    var u_base = 'https://kitsu.io/api/edge/characters/'+id+'/media-characters';
    var i_rq = new XMLHttpRequest();
    i_rq.onload = function() {
        if (this.status == 200) {
            var info = JSON.parse(this.response);
            data = info.data[0];
            i_get_media(data.id);          
        } else {
            console.log("Some Error... ?");
        }
    };
    i_rq.open('GET', u_base, true);
    i_rq.send();
}

function ins_mdl(i){
    o_mdl.classList.add('in');

    const o_title = document.getElementById('mdl-title');
    o_title.textContent = i.attributes.name;
    
    const o_info = document.getElementById('mdl-info');
    o_info.innerHTML = i.attributes.description;

    
    const o_img = document.getElementById('mdl-img');
    o_img.width = 250;
    if(i.attributes.image == null){
        o_img.src = 'library/img/broken-img.svg';
    } else {
        o_img.src = i.attributes.image.original;
    }
    o_img.title = i.attributes.name;

    o_mdl.classList.add('in');

    i_get_relation(i.id);
}

function mdl_close(){
    o_mdl.classList.remove('in');
    // if(o_mdl.classList.contains('in')){}
}

function i_get_id(id){

    var u_base = 'https://kitsu.io/api/edge/characters?filter[id]='+id;

    var i_rq = new XMLHttpRequest();
    i_rq.onload = function() {
        if (this.status == 200) {
            var info = JSON.parse(this.response);

            data = info.data[0];
            ins_mdl(data);          
        } else {
            console.log("ID it's Right ?");
        }
    };
    i_rq.open('GET', u_base, true);
    i_rq.overrideMimeType('text/xml; charset=utf-8');
    i_rq.send();
}


/* Start Table List */
i_get_list(10, 0, '');